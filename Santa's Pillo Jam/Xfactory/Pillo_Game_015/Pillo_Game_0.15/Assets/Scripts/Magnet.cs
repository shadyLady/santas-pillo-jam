﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public delegate void VoidDelegate();

public class Magnet : MonoBehaviour 
{
    [SerializeField]
    private Block _heldBlock;

    [SerializeField]
    private float _batteryTime;
    private float _batteryTimer;


    [SerializeField]
    private GameObject _cloudImage;
    [SerializeField]
    private Text _timerText;

    [SerializeField]
    private PackageSpawner _spawner; 

    private void Update()
    {
        _timerText.text = Mathf.RoundToInt((_batteryTimer - Time.time)).ToString();
        _cloudImage.transform.position = transform.position + new Vector3(3f, 1f);


        if(_heldBlock != null)
        {
            DragBlock();
            DrainBattery();
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (_heldBlock != null)
            return; 

        Block hitBlock = col.gameObject.GetComponent<Block>();

        if(hitBlock != null)
        {
            if(!hitBlock.IsPositioned)
            {
                _heldBlock = hitBlock;
                _heldBlock.GetComponent<Rigidbody2D>().gravityScale = 0;
                _heldBlock.HorizontalSpeed = 0.0f;
                _heldBlock.OnBlockPlaced += ResetMagnet;
                _batteryTimer = Time.time + _batteryTime;
                _cloudImage.gameObject.SetActive(true);


            }
        }
    }
    private void DragBlock()
    {
        _heldBlock.transform.position = new Vector3(transform.position.x, transform.position.y - 1.9f);
    }
    private void DrainBattery()
    {
        if(Time.time > _batteryTimer)
        {
            Debug.Log("Triggered");
            ResetMagnet();
        }
    }
    private void ResetMagnet()
    {
        Debug.Log("Released block!");
        _heldBlock.OnBlockPlaced -= ResetMagnet;
        _heldBlock.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
        _spawner.RemoveBandedPackage(_heldBlock.gameObject);
        _heldBlock = null;
        _cloudImage.gameObject.SetActive(false);

        _batteryTimer = _batteryTime; 
    }
}
