﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sleigh : MonoBehaviour 
{
    [SerializeField]
    private List<Block> _stackedPackages = new List<Block>();

    public void AddPackageToList(Block package)
    {
        if(_stackedPackages.Count > 0)
        {
            if (_stackedPackages.Contains(package))
                return; 
        }

        _stackedPackages.Add(package);
    }
    public void RemovePackageFromList(Block package)
    {
        if(_stackedPackages.Count > 0)
        {
            for(int i = 0; i < _stackedPackages.Count; i++)
            {
                if(_stackedPackages[i] == package)
                {
                    Destroy(package);
                    _stackedPackages.Remove(package);
                    break;
                }
            }
        }
    }
    public void ClearList()
    {
        if(_stackedPackages.Count > 0)
        {
            for(int i = 0; i < _stackedPackages.Count; i++)
            {
                _stackedPackages[i].gameObject.tag = "Sleigh";
                _stackedPackages[i].RemoveRigidbody();
                Destroy(_stackedPackages[i]);
            }
        }

        _stackedPackages.Clear();
    }
}
