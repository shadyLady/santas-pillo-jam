﻿using UnityEngine;
using System.Collections;

public class DustCloud : MonoBehaviour
{
    [SerializeField]
    private float _animationDuration;

    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Hit something!");
        if(col.gameObject.tag == "Gift")
        {
            _spriteRenderer.enabled = true;
            StartCoroutine(DisableCloud());
        }
    }

    private IEnumerator DisableCloud()
    {
        yield return new WaitForSeconds(_animationDuration);

        _spriteRenderer.enabled = false;
    }
}
