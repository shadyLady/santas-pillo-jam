﻿using UnityEngine;
using System.Collections;

public class InterfaceManager : MonoBehaviour 
{
    [SerializeField]
    private GameObject _warningSign;

    [SerializeField]
    private GameObject _menuHUD;

    [SerializeField]
    private GameObject _gameHUD;

    private void Start()
    {
        GameManager.Instance.OnPackageLost += WarnPlayer;
        GameManager.Instance.OnGameStart += HideMenuHUD;
        GameManager.Instance.OnGameStart += ShowGameHUD;
        GameManager.Instance.OnGameEnd += HideMenuHUD;
    }

    private void WarnPlayer()
    {
        StartCoroutine(ShowWarning(0.2f, 8));
    }
    private IEnumerator ShowWarning(float duration, int repeats)
    {
        while(repeats > 0)
        {
            _warningSign.SetActive(!_warningSign.activeSelf);
            repeats--;
            yield return new WaitForSeconds(duration);
        }

        yield return null;
    }

    private void HideMenuHUD()
    {
        _menuHUD.SetActive(false);
    }

    private void ShowGameHUD()
    {
        _gameHUD.SetActive(true);
    }

    private void HideGameHUD()
    {
        _gameHUD.SetActive(false);
    }
}
