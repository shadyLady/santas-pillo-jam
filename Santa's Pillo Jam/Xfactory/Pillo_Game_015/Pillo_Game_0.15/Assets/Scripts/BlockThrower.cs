﻿using UnityEngine;
using System.Collections;

public class BlockThrower : MonoBehaviour 
{
    private float _movespeed = 8;

    private float _dropDelay = 5.0f;
    private float _dropTimer;

    [SerializeField]
    private Animator _animBob;
    [SerializeField]
    private Animator _animReina;

    private AudioSource _audioSource;

    private Vector3 _motion;
    private void Awake()
    {
        _dropTimer = Time.time + _dropDelay;

        PilloController.ConfigureSensorRange(0x50, 0x6f);
        _audioSource = GetComponent<AudioSource>();
    }
    private void Update()
    {
        float h = PilloController.GetSensor(Pillo.PilloID.Pillo1);
        float v = PilloController.GetSensor(Pillo.PilloID.Pillo2);

        float kh = Input.GetAxis("Horizontal");
        float kv = Input.GetAxis("Vertical");

        Move(kh, -kv);

        if (Mathf.Abs(_motion.x) > 0f || Mathf.Abs(_motion.y) > 0f)
        {
            _audioSource.enabled = true;
            _audioSource.mute = false;
        }
        else _audioSource.mute = true;
    } 

    private void Move(float h, float v)
    {
        _motion.x = h * _movespeed * Time.deltaTime;
        _motion.y = -v * _movespeed * Time.deltaTime;

        if (h <= 0)
        {
            _motion.x = -_movespeed * Time.deltaTime;
        }

        if (v <= 0)
        {
            _motion.y = _movespeed * Time.deltaTime;
        }

        transform.position += _motion;

        _animBob.SetFloat("pillo1Input", h);
        _animReina.SetFloat("pillo2Input", v);

        ClampThrower();


    }
    private void ClampThrower()
    {
        Vector3 currentPosition = transform.position;

        float topRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, currentPosition.z - Camera.main.transform.position.z)).y - 9f;
        float bottomRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).y;
        float leftRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).x + 6f;
        float rightRestriction = Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).x;

        if (currentPosition.y > topRestriction) { currentPosition.y = topRestriction; _motion.y = 0; }
        else if (currentPosition.y < bottomRestriction) { currentPosition.y = bottomRestriction; _motion.y = 0; }

        if (currentPosition.x > rightRestriction) { currentPosition.x = rightRestriction; _motion.x = 0; }
        else if (currentPosition.x < leftRestriction) { currentPosition.x = leftRestriction; _motion.x = 0; }

        transform.position = currentPosition;
    }
}
