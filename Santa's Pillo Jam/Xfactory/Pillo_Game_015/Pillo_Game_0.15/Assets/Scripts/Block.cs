﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour 
{
    [SerializeField]
    private float _fallspeed = 5.0f;

    [SerializeField]
    private float _horizontalSpeed = 3.0f;
    public float HorizontalSpeed
    {
        get { return _horizontalSpeed; }
        set { _horizontalSpeed = value; }
    }
 
    private Rigidbody2D _rb;

    [SerializeField]
    private bool _isPositioned = false; 
    public bool IsPositioned
    {
        get { return _isPositioned; }
        set { _isPositioned = value; }
    }

    private event VoidDelegate _onBlockPlaced; 
    public VoidDelegate OnBlockPlaced
    {
        get { return _onBlockPlaced; }
        set { _onBlockPlaced = value; }
    }

    private event GiftPerisher _onBlockLost;
    public GiftPerisher OnBlockLost
    {
        get { return _onBlockLost; }
        set { _onBlockLost = value; }
    }

    private Sleigh _sleigh;
    public Sleigh Sleigh
    {
        get { return _sleigh; }
        set { _sleigh = value; }
    }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        transform.position += new Vector3(_horizontalSpeed * Time.deltaTime, 0.0f);

        CheckBottomPosition();
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Sleigh" || (col.gameObject.tag == "Gift" && col.gameObject.GetComponent<Block>().IsPositioned))
        {
            if (_onBlockPlaced != null)
                _onBlockPlaced();

            _sleigh.AddPackageToList(this);
            _isPositioned = true;
            _rb.constraints = RigidbodyConstraints2D.None;
            GameManager.Instance.BlockCount += 1;
            GameManager.Instance.TestHighestBlock(transform);
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Blockade")
        {
            _horizontalSpeed = 0.0f; 
        }
    }
    private void MissedBlock()
    {
        GameManager.Instance.MissedBlocks += 1;
        _sleigh.RemovePackageFromList(this);

        if (_onBlockLost != null)
            _onBlockLost(gameObject);

        Destroy(gameObject);
    }
    private void CheckBottomPosition()
    {
        Vector3 currentPosition = transform.position;

        float bottomRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).y;

        if (currentPosition.y < bottomRestriction)
        {
            MissedBlock();
        }
    }
    private void OnCollisionExit2D(Collision2D col)
    {
        Debug.Log("Left collider");
        if(col.gameObject.tag == "Conveyor")
        {
            Debug.Log("Conveyor ignored");
            Physics2D.IgnoreCollision(col.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
    public void RemoveRigidbody()
    {
        Destroy(_rb);
    }

}
