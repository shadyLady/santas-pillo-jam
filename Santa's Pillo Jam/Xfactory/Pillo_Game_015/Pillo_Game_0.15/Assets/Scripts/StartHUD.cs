﻿using UnityEngine;
using System.Collections;

public class StartHUD : MonoBehaviour 
{
    private bool _pillo1Pressed = false;
    private bool _pillo2Pressed = false;

    [SerializeField]
    private GameObject _portretBob;
    [SerializeField]
    private GameObject _portretReina;
    [SerializeField]
    private GameObject _dropZone;
    [SerializeField]
    private GameObject _dropHere;
    [SerializeField]
    private GameObject _vinkBob;
    [SerializeField]
    private GameObject _vinkReina;

    void Start()
    {
        PilloController.ConfigureSensorRange(0x50, 0x6f);
        _vinkBob.SetActive(false);
        _vinkReina.SetActive(false);
        _dropZone.SetActive(false);
        _dropHere.SetActive(false);
    }

    void Update()
    {
        float h = PilloController.GetSensor(Pillo.PilloID.Pillo1);
        float v = PilloController.GetSensor(Pillo.PilloID.Pillo2);

        float kh = Input.GetAxis("Horizontal");
        float kv = Input.GetAxis("Vertical");

        if (kh != 0)
        {
            _vinkBob.SetActive(true);
            _pillo1Pressed = true;
        }
        if (kv != 0)
        {
            _vinkReina.SetActive(true);
            _pillo2Pressed = true;
        }

        if (_pillo1Pressed && _pillo2Pressed)
        {
            GameManager.Instance.StartGame();
        }

        Debug.Log(_pillo1Pressed);
        Debug.Log(_pillo2Pressed);
    }
}
