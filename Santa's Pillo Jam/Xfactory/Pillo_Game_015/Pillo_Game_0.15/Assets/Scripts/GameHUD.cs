﻿using UnityEngine;
using System.Collections;

public class GameHUD : MonoBehaviour 
{
    [SerializeField]
    private GameObject[] _packagesLost; 

    private void Awake()
    {
        GameManager.Instance.OnPackageLost += UpdateHUD;
    }

    private void UpdateHUD()
    {
        int packagesLost = GameManager.Instance.MissedBlocks;
        Debug.Log(packagesLost + " " + _packagesLost.Length);

        if(packagesLost < _packagesLost.Length)
        {
            _packagesLost[packagesLost - 1].SetActive(true);
        }
    }

}
