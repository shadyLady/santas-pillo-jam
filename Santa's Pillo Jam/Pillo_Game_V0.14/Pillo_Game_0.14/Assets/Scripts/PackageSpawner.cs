﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public delegate void GiftPerisher(GameObject gift);

public class PackageSpawner : MonoBehaviour 
{
    [SerializeField]
    private GameObject[] _packages;

    [SerializeField]
    private float _spawnDelay;
    private float _spawnTimer = 0.0f;

    private GameObject _nextPackage;
    public GameObject NextPackage
    {
        get { return _nextPackage; }
        set { _nextPackage = value; }
    }

    [SerializeField]
    private Transform _spawnPosition;

    [SerializeField]
    private Animator _animator; 

    [SerializeField]
    private float _animationDuration;

    [SerializeField]
    private Sleigh _sleigh;

    [SerializeField]
    private List<GameObject> _bandedPackage = new List<GameObject>();

    private GameObject GetRandomPackage()
    {
        return _packages[Random.Range(0, _packages.Length)];
    }
    private void Update()
    {
        if(_nextPackage == null)
        {
            if (_bandedPackage.Count < 1)
            {
                StartCoroutine(SpawnPackage());
            }
        }
    }


    public void RemoveBandedPackage(GameObject package)
    {
        if (_bandedPackage.Count > 0)
        {
            for (int i = 0; i < _bandedPackage.Count; i++)
            {
                if (_bandedPackage[i] == package)
                {
                    _bandedPackage.Remove(package);
                    break;
                }
            }
        }
    }
    private IEnumerator SpawnPackage()
    {
        Debug.Log("Started");
        _nextPackage = GetRandomPackage();

        _animator.SetTrigger("SpawnPackage");
        yield return new WaitForSeconds(_animationDuration);

        GameObject package = Instantiate(_nextPackage, _spawnPosition.position, Quaternion.identity) as GameObject;
        _bandedPackage.Add(package);
        Block packageScript = package.GetComponent<Block>();
        packageScript.OnBlockLost += RemoveBandedPackage;
        packageScript.Sleigh = _sleigh;

        _nextPackage = null;
    }


}
