﻿using UnityEngine;
using System.Collections;

public class InterfaceManager : MonoBehaviour 
{
    [SerializeField]
    private GameObject _warningSign;

    private void Start()
    {
        GameManager.Instance.OnPackageLost += WarnPlayer;
    }
    private void WarnPlayer()
    {
        StartCoroutine(ShowWarning(0.2f, 8));
    }
    private IEnumerator ShowWarning(float duration, int repeats)
    {
        while(repeats > 0)
        {
            _warningSign.SetActive(!_warningSign.activeSelf);
            repeats--;
            yield return new WaitForSeconds(duration);
        }

        yield return null;
    }
}
