﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
    private Vector3 _startPosition;
    private Vector3 _targetPosition; 
    private void Start()
    {

        _startPosition = transform.position;
        GameManager.Instance.OnGameEnd += ShowResult;
    }

    private void Update()
    {
        TrackHighestBlock();
    }

    private void ShowResult()
    {
        Debug.Log("Zooming out to result screen");
    }

    private void TrackHighestBlock()
    {
        if(GameManager.Instance.HighestBlock != null)
        {
            Debug.Log(GameManager.Instance.HighestBlock.position.y);
            _targetPosition = GameManager.Instance.HighestBlock.position;

            if(_targetPosition.y > transform.position.y)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, _targetPosition.y, transform.position.z), 0.01f);
                //StartCoroutine(LerpToPosition(_targetPosition));
            }
        }
    }

    private IEnumerator LerpToPosition(Vector3 _targetPosition)
    {
        float lerpTimer = 0.0f;

        while (lerpTimer < 1.0f)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, _targetPosition.y), lerpTimer);
            lerpTimer += 0.1f;

            yield return new WaitForEndOfFrame();
        }
    }
}
