﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : Component 
{
    private static T _instance; 
    public static T Instance
    {
        get { return _instance; }
    }

    protected virtual void Awake()
    {
        if(_instance != null)
        {
            Destroy(gameObject);
        }

        _instance = this as T;
    }
}
