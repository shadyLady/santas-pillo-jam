﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    STATE_MENU,
    STATE_GAME,
    STATE_RESULT
};

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private Transform _highestBlock;
    public Transform HighestBlock
    {
        get { return _highestBlock; }
        set { _highestBlock = value; }
    }

    [SerializeField]
    private int[] _milestones;
    private int _levelPosition = 0;

    private int _blockCount; 
    public int BlockCount
    {
        get { return _blockCount; }
        set { _blockCount = value; }
    }

    [SerializeField]
    private int _missedBlocks; 
    public int MissedBlocks
    {
        get { return _missedBlocks; }
        set 
        {
            _missedBlocks = value;
            CheckOnSurvival();

            if (_onPackageLost != null)
                _onPackageLost();
        }
    }

    private GameState _gameState; 
    public GameState GameState
    {
        get { return _gameState; }
    }

    private event VoidDelegate _onGameStart;
    public VoidDelegate OnGameStart
    {
        get { return _onGameStart; }
        set { _onGameStart = value; }
    }

    private event VoidDelegate _onGameEnd;
    public VoidDelegate OnGameEnd
    {
        get { return _onGameStart; }
        set { _onGameEnd = value; }
    }

    private event VoidDelegate _onPackageLost; 
    public VoidDelegate OnPackageLost
    {
        get { return _onPackageLost; }
        set { _onPackageLost = value; }
    }

    [SerializeField]
    private Sleigh _sleigh; 

    protected override void Awake()
    {
        base.Awake();

        _gameState = GameState.STATE_MENU;
    }
    public void StartGame()
    {
        if (_gameState != GameState.STATE_MENU)
            return;

        if (_onGameStart != null)
            _onGameStart();

        _gameState = GameState.STATE_GAME;
    }
    public void EndGame()
    {
        if (_gameState != GameState.STATE_GAME)
            return;

        if (_onGameEnd != null)
            _onGameEnd();

        _gameState = GameState.STATE_RESULT;
    }
    public void TestHighestBlock(Transform testedBlock)
    {
        if (_highestBlock == null)
            _highestBlock = testedBlock;

        if(_levelPosition < _milestones.Length)
        {
            if (_highestBlock.position.y >= _milestones[_levelPosition])
            {
                Debug.Log("Milestone Reached");
                _sleigh.ClearList();
                _levelPosition++;
            }
        }

        if(testedBlock.position.y > _highestBlock.position.y)
        {
            Debug.Log("New high point!");
            _highestBlock = testedBlock;
        }
    }
    private void CheckOnSurvival()
    {
        if(_missedBlocks >= 3)
        {
            Debug.Log("Game Over");
            _gameState = GameState.STATE_RESULT;
        }
    }
}
