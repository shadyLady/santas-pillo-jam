﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class PackageSpawner : MonoBehaviour 
{
    [SerializeField]
    private GameObject[] _packages;

    [SerializeField]
    private float _spawnDelay;
    private float _spawnTimer = 0.0f;

    private GameObject _nextPackage;

    [SerializeField]
    private Transform _spawnPosition;

    private GameObject GetRandomPackage()
    {
        return _packages[Random.Range(0, _packages.Length)];
    }
    private void Update()
    {
        RunTimer();
    }

    private void RunTimer()
    {
        if(Time.time > _spawnTimer)
        {
            // TODO sucks
            _nextPackage = GetRandomPackage();
            Instantiate(_nextPackage, _spawnPosition.position, Quaternion.identity);

            //Camera.main.rect.max;

            _spawnTimer += _spawnDelay;
        }
    }
}
