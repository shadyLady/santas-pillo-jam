﻿using UnityEngine;
using System.Collections;

public class BlockThrower : MonoBehaviour 
{
    private float _movespeed = 8;

    private float _dropDelay = 5.0f;
    private float _dropTimer;

    [SerializeField]
    private Vector2 _originPoint;

    private void Awake()
    {
        _dropTimer = Time.time + _dropDelay;

        PilloController.ConfigureSensorRange(0x50, 0x6f);

    }
    private void Update()
    {
        Vector3 motion = Vector3.zero;

        // suks
        float h = PilloController.GetSensor(Pillo.PilloID.Pillo1);
        float v = PilloController.GetSensor(Pillo.PilloID.Pillo2);

        float kh = Input.GetAxis("Horizontal");
        float kv = Input.GetAxis("Vertical");

        // also sucks
        motion.x = h * _movespeed * Time.deltaTime;
        motion.y = -v * _movespeed * Time.deltaTime;

        if(h <= 0 && transform.position.x > _originPoint.x)
        {
            motion.x = -_movespeed * Time.deltaTime; 
        }

        if(v <= 0 && transform.position.y < _originPoint.y)
        {
            motion.y = _movespeed * Time.deltaTime;
        }

        transform.position += motion;

        ClampThrower();
    } 

    private void ClampThrower()
    {
        Vector3 currentPosition = transform.position;

        float topRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, currentPosition.z - Camera.main.transform.position.z)).y;
        float bottomRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).y;
        float leftRestriction = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).x;
        float rightRestriction = Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0.0f, currentPosition.z - Camera.main.transform.position.z)).x;

        if (currentPosition.y > topRestriction) { currentPosition.y = topRestriction; }
        else if (currentPosition.y < bottomRestriction) { currentPosition.y = bottomRestriction; }

        if (currentPosition.x > rightRestriction) { currentPosition.x = rightRestriction; }
        else if (currentPosition.x < leftRestriction) { currentPosition.x = leftRestriction; }

        //Debug.Log(bottomRestriction);
        transform.position = currentPosition;
    }
}
