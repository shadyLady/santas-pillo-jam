﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    [SerializeField]
    private Transform _highestBlock; 

    public void TestHighestBlock(Transform testedBlock)
    {
        if (_highestBlock == null)
            _highestBlock = testedBlock;

        if(testedBlock.position.y > _highestBlock.position.y)
        {
            Debug.Log("New high point!");
            _highestBlock = testedBlock;
        }
    }
}
