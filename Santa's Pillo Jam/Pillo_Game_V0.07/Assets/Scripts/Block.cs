﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour 
{
    [SerializeField]
    private float _fallspeed = 5.0f;

    [SerializeField]
    private float _horizontalSpeed = 3.0f;
    public float HorizontalSpeed
    {
        get { return _horizontalSpeed; }
        set { _horizontalSpeed = value; }
    }
 
    private Rigidbody2D _rb;

    [SerializeField]
    private bool _isPositioned = false; 
    public bool IsPositioned
    {
        get { return _isPositioned; }
        set { _isPositioned = value; }
    }

    private event VoidDelegate _onBlockPlaced; 
    public VoidDelegate OnBlockPlaced
    {
        get { return _onBlockPlaced; }
        set { _onBlockPlaced = value; }
    }

    private GameManager _gameManager;

    private void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        transform.position += new Vector3(_horizontalSpeed * Time.deltaTime, 0.0f);
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Sleigh" || (col.gameObject.tag == "Gift" && col.gameObject.GetComponent<Block>().IsPositioned))
        {
            if (_onBlockPlaced != null)
                _onBlockPlaced();

            _isPositioned = true;
            _rb.constraints = RigidbodyConstraints2D.None;
            //_gameManager.TestHighestBlock(this.transform);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Blockade")
        {
            _horizontalSpeed = 0.0f; 
        }
    }
}
