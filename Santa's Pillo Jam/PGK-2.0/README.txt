README

In this document we share important information regarding the use of the Pillo Game Key System (PGK).

//1. What does the PGK contain?
1. KeyManager.dll
2. Example project for Unity
3. Documentation on PGK
4. README.txt

//2. Copyright notice
PGK 1.1 is created on 02-04-2015 by Plus-Games, Copyright is reserverd to PILLO Games.
PGK 2.0, created on 02-06-2015 holds an optimized version of PGK 1.1, Unity 5 implementation by Joris Kauffeld, art by Vincent Wolters.
PILLO is a registered trademark. if you'd like to use PILLO in referral to your game, contact us with the adress below.

//3. Company info
Company: Pillo Games - by Ard Jacobs IxD, Vestdijk 133, 5611 CB Eindhoven, The Netherlands
Website: www.pillogames.com
E-mail 1: info@pillogames.com - for general questions & remarks
E-mail 2: bugreport@pillogames.com - for software related issues & comments